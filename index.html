<!DOCTYPE html>
<html lang="en">

<head>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZEYNJ951R3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ZEYNJ951R3');
</script>

<meta charset="utf-8" />
<link href="https://fonts.googleapis.com/css?family=Oxygen&display=swap" rel="stylesheet">
<link href="main.css" rel="stylesheet" type="text/css" media="screen">
<link href="./media/icon.png" type="image/png" rel="shortcut icon"/>

<title>Torch</title>

</head>

<body>
<div class="header">
  <h1>Torch<img src="./media/logo.png" alt="logo" width="50px"/></h1>
  
</div>
<!--h1>TORCH</h1> <img src="icon.png" width="80px-->

<p>
Torch is a code to simulate coupled gas and N-body dynamics in astrophysical
systems such as newly forming star clusters.
It combines
the <a href="https://flash.rochester.edu/site/index.shtml">FLASH</a> code for gas dynamics
and the ph4 code for direct N-body evolution
via the <a href="http://www.amusecode.org">AMUSE</a> framework.
An introduction to Torch is given by
Wall <span style='font-style:italic'>et al.</span>
<a href="https://ui.adsabs.harvard.edu/abs/2019ApJ...887...62W/abstract">2019ApJ...887...62W</a>
and <a href="https://ui.adsabs.harvard.edu/abs/2020ApJ...904..192W/abstract">2020ApJ...904..192W</a>,
as well as Joshua Wall's
<a href="https://ui.adsabs.harvard.edu/abs/2019PhDT.........2W/abstract">Ph.D thesis</a>
(<a href="https://www.dropbox.com/s/c0qh1no85y1jnxf/J_Wall-Thesis_final.pdf?dl=0">pdf mirror</a>).
</p>

<p>
Torch is distributed as an open-source
<a href="https://bitbucket.org/torch-sf/torch">Bitbucket repository</a>.
It is a new code in active development; contributions and bug reports are
welcome.
</p>

<p>
To start using Torch, here is a <a href="quickstart.pdf">quickstart guide</a>
adapted from a workshop held 2019 August 28-30.
</p>

<p>
You can join the
<a href="https://groups.google.com/forum/#!aboutgroup/torch-users">Torch mailing list</a>
to discuss Torch usage and development.
</p>

<p>
There are several research papers that utilize the Torch framework.
<p>
Pre-print:
<ul>
  <li>Polak <i>et al.</i> (2024c): <a href="https://arxiv.org/abs/2408.14592">Massive star cluster formation III. Early mass segregation during cluster assembly</a></li>
  
</ul>
Published:
<ul>
  <li>Cournoyer-Cloutier <i>et al.</i> (2024b): <a href="https://ui.adsabs.harvard.edu/abs/2024ApJ...977..203C/abstract">Massive Star Cluster Formation with Binaries. I. Evolution of Binary Populations</a></li>
  <li>Polak <i>et al.</i> (2024b): <a href="https://ui.adsabs.harvard.edu/abs/2024A%26A...690A.207P/abstract">Massive star cluster formation: II. Runaway stars as fossils of sub-cluster mergers</a></li>
  <li>Polak <i>et al.</i> (2024a): <a href="https://ui.adsabs.harvard.edu/abs/2024A%26A...690A..94P/abstract">Massive star cluster formation: I. High star formation efficiency while resolving feedback of individual stars</a></li>
  <li>Cournoyer-Cloutier <i>et al.</i> (2023): <a href="https://ui.adsabs.harvard.edu/abs/2023MNRAS.521.1338C/abstract">Early evolution and three-dimensional structure of embedded star clusters</a></li>
  <li>Wilhelm <i>et al.</i> (2023): <a href="https://ui.adsabs.harvard.edu/abs/2023MNRAS.520.5331W/abstract">Radiation shielding of protoplanetary discs in young star-forming regions</a></li>
  <li>Lewis <i>et al.</i> (2023): <a href="https://ui.adsabs.harvard.edu/abs/2023ApJ...944..211L/abstract">Early-forming massive stars supress star formation and heirarchical cluster assembly</a></li>
  <li>Cournoyer-Cloutier <i>et al.</i> (2021): <a href="https://ui.adsabs.harvard.edu/abs/2021MNRAS.501.4464C/abstract">Implementing primordial binaries in simulations of star cluster formation with a hybrid MHD and direct N-body method</a></li>
  <li>Wall <i>et al.</i> (2020): <a href="https://ui.adsabs.harvard.edu/abs/2020ApJ...904..192W/abstract">Modeling the effects of stellar feedback during star cluster formation using a hybrid gas and N-body method</a></li>
  <li>Wall <i>et al.</i> (2019): <a href="https://ui.adsabs.harvard.edu/abs/2019ApJ...887...62W/abstract">Collisional N-body dynamics coupled to self-gravitating magnetohydrodynamics reveals dynamical binary formation</a></li>
</ul>
</p>

<p>
Torch simulations are also used as initial conditions for N-body and hydrodynamics simulations.
<p>
Published:
<ul>
    <li>Cournoyer-Cloutier <i>et al.</i> (2024a): <a href="https://ui.adsabs.harvard.edu/abs/2024ApJ...975..207C/abstract">Binary Disruption and Ejected Stars from Hierarchical Star Cluster Assembly</a></li>
</ul>
</p>

<p>
Torch is a central part of several PhD and MSc theses:
<ul>
  <li>Joshua E. Wall (PhD, 2019): <a href="https://ui.adsabs.harvard.edu/abs/2019PhDT.........2W/abstract">The Formation and Early Evolution of Stellar Clusters</a></li>
  <li>Claude Cournoyer-Cloutier (MSc, 2021): <a href="http://hdl.handle.net/11375/26924">Dynamical Modification of a Primordial Population of Binaries in Simulations of Star Cluster Formation</a></li>
  <li>Sabrina M. Appel (PhD, 2024): <a href="https://doi.org/doi:10.7282/t3-p3kg-dk21">Exploring the effect of protostellar feedback on star formation and molecular cloud dynamics</a></li>
  <li>Brooke Polak (PhD, 2024): <a href="https://archiv.ub.uni-heidelberg.de/volltextserver/35490/">The Secret Lives of Young Massive Star Clusters</a></li>
</ul>
</p>

<p>
The Torch project is supported by
NASA grant no.&nbsp;NNX14AP27G and
NSF grant no.&nbsp;<a href="https://nsf.gov/awardsearch/showAward?AWD_ID=1815461">1815461</a>.
</p>

</body>

</html>
